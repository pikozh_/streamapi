import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilterByCritery {

    public static List<Student> getStudentsByFaculty(ArrayList<Student> listOfStudents, String faculty) {
        return listOfStudents
                .stream()
                .filter((p) -> p.getFaculty().equals(faculty))
                .collect(Collectors.toList());
    }


    public static List<Student> getStudentsByDateOfBirth(ArrayList<Student> listOfStudents, int dateOfBirth) {
        return listOfStudents
                .stream()
                .filter((p) -> p.getYearOfBirth() > dateOfBirth)
                .collect(Collectors.toList());
    }

    public static List<String> getStudentsByGroupWithFormat(ArrayList<Student> listOfStudents, String group) {
        return listOfStudents
                .stream()
                .filter((p) -> p.getGroup().equals(group))
                .map(student -> student.getLastName() + " " + student.getFirstName() + " " + student.getPatronymic())
                .collect(Collectors.toList());
    }

    public static List<String> getStudentsWithFormat(ArrayList<Student> listOfStudents) {
        return listOfStudents
                .stream()
                .map(student -> student.getLastName() + " " + student.getFirstName() + " " + student.getPatronymic() + " - " + student.getFaculty() + " " + student.getGroup())
                .collect(Collectors.toList());
    }

    public static int getCountOfStudentsByGroup(ArrayList<Student> listOfStudents, String group) {
        return (int) listOfStudents
                .stream()
                .filter((student -> student.getGroup().equals(group)))
                .count();
    }

}

