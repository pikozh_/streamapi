import java.util.ArrayList;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {

        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(1, "Vasya", "Pipkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DB"));
        students.add(new Student(2, "Vasya", "Pupkin", "Vasylych", 1998, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DB"));
        students.add(new Student(3, "Vasya", "Popkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Shmitrosti", "Interesnyi", "DBL"));
        students.add(new Student(4, "Vasya", "Papkin", "Vasylych", 1999, "Pupochnaya st.", "0987654433", "Ofigennoosti", "Interesnyi", "DB"));
        students.add(new Student(5, "Vasya", "Pepkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DB"));
        students.add(new Student(6, "Vasya", "Pupkun", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Electronics", "Interesnyi", "DB"));
        students.add(new Student(7, "Vasya", "Bipkin", "Vasylych", 1998, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DB"));
        students.add(new Student(8, "Vasya", "Shmipkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Shmitrosti", "Interesnyi", "DBL"));
        students.add(new Student(9, "Vasya", "Napkin", "Vasylych", 1999, "Pupochnaya st.", "0987654433", "Ofigennoosti", "Interesnyi", "DB"));
        students.add(new Student(10, "Vasya", "Pipkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DBK"));

        Scanner scanner = new Scanner(System.in);

//        System.out.println("Введите название факультета, с которого желаете получить список студентов:");
//        String nameOfFaculty = scanner.nextLine();
//        System.out.println(FilterByCritery.getStudentsByFaculty(students, nameOfFaculty));
//
//        System.out.println("Введите год и будет выведен список студентов, родившихся после заданного года:");
//        int year = Integer.parseInt(scanner.nextLine());
//        System.out.println(FilterByCritery.getStudentsByDateOfBirth(students, year));
//
//        System.out.println("Введите название учебной группы, после чего будет выведен список группы в формате Фамилия, Имя, Отчество :");
//        String nameOfGroup = scanner.nextLine();
//        System.out.println(FilterByCritery.getStudentsByGroupWithFormat(students, nameOfGroup));
//
//        System.out.println("Сейчас будет выведен список студентов в формате Фамилия, Имя, Отчество - Факультет, Группа");
//        System.out.println(FilterByCritery.getStudentsWithFormat(students));

        System.out.println("Введите название группы в которой хотите узнать количество студентов:");
        String nameOfGroupToCount = scanner.nextLine();
        System.out.println(FilterByCritery.getCountOfStudentsByGroup(students, nameOfGroupToCount));
    }
}
