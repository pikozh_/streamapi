import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class FilterByCriteryTest {

    @Test
    public void getStudentsByFaculty_ShouldPass(){
        //given
        String faculty = "Hitrosti";

        //when
        String expected = "[Student{id=1, firstName='Vasya', lastName='Pipkin', patronymic='Vasylych', yearOfBirth=1997, address='Pupochnaya st.', telephone='0987654433', faculty='Hitrosti', course='Interesnyi', group='DB'}, " +
                "Student{id=2, firstName='Vasya', lastName='Pupkin', patronymic='Vasylych', yearOfBirth=1998, address='Pupochnaya st.', telephone='0987654433', faculty='Hitrosti', course='Interesnyi', group='DB'}, " +
                "Student{id=5, firstName='Vasya', lastName='Pepkin', patronymic='Vasylych', yearOfBirth=1997, address='Pupochnaya st.', telephone='0987654433', faculty='Hitrosti', course='Interesnyi', group='DB'}, " +
                "Student{id=7, firstName='Vasya', lastName='Bipkin', patronymic='Vasylych', yearOfBirth=1998, address='Pupochnaya st.', telephone='0987654433', faculty='Hitrosti', course='Interesnyi', group='DB'}, " +
                "Student{id=10, firstName='Vasya', lastName='Pipkin', patronymic='Vasylych', yearOfBirth=1997, address='Pupochnaya st.', telephone='0987654433', faculty='Hitrosti', course='Interesnyi', group='DBK'}]";
        String actual = FilterByCritery.getStudentsByFaculty(createListOfStudents(),faculty).toString();

        //then
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void getStudentsByDateOfBirth_ShouldPass(){
        //given
        int yearOfBirth = 1997 ;

        //when
        String expected = "[Student{id=2, firstName='Vasya', lastName='Pupkin', patronymic='Vasylych', yearOfBirth=1998, address='Pupochnaya st.', telephone='0987654433', faculty='Hitrosti', course='Interesnyi', group='DB'}, " +
                "Student{id=4, firstName='Vasya', lastName='Papkin', patronymic='Vasylych', yearOfBirth=1999, address='Pupochnaya st.', telephone='0987654433', faculty='Ofigennoosti', course='Interesnyi', group='DB'}, " +
                "Student{id=7, firstName='Vasya', lastName='Bipkin', patronymic='Vasylych', yearOfBirth=1998, address='Pupochnaya st.', telephone='0987654433', faculty='Hitrosti', course='Interesnyi', group='DB'}, " +
                "Student{id=9, firstName='Vasya', lastName='Napkin', patronymic='Vasylych', yearOfBirth=1999, address='Pupochnaya st.', telephone='0987654433', faculty='Ofigennoosti', course='Interesnyi', group='DB'}]";
        String actual = FilterByCritery.getStudentsByDateOfBirth(createListOfStudents(),yearOfBirth).toString();

        //then
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void getStudentsByGroupWithFormat_ShouldPass(){
        //given
        String group = "DBL" ;

        //when
        String expected = "[Popkin Vasya Vasylych, Shmipkin Vasya Vasylych]";
        String actual = FilterByCritery.getStudentsByGroupWithFormat(createListOfStudents(),group).toString();

        //then
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void getStudentsWithFormat_ShouldPass(){
        //when
        String expected = "[Pipkin Vasya Vasylych - Hitrosti DB, " +
                "Pupkin Vasya Vasylych - Hitrosti DB, " +
                "Popkin Vasya Vasylych - Shmitrosti DBL, " +
                "Papkin Vasya Vasylych - Ofigennoosti DB, " +
                "Pepkin Vasya Vasylych - Hitrosti DB, " +
                "Pupkun Vasya Vasylych - Electronics DB, " +
                "Bipkin Vasya Vasylych - Hitrosti DB, " +
                "Shmipkin Vasya Vasylych - Shmitrosti DBL, " +
                "Napkin Vasya Vasylych - Ofigennoosti DB, " +
                "Pipkin Vasya Vasylych - Hitrosti DBK]";
        String actual = FilterByCritery.getStudentsWithFormat(createListOfStudents()).toString();

        //then
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void getCountOfStudentsByGroup_ShouldPass(){
        //given
        String group = "DB";

        //when
        int expected = 7;
        int actual = FilterByCritery.getCountOfStudentsByGroup(createListOfStudents(), group);

        //then
        Assert.assertEquals(expected,actual);
    }



    public ArrayList<Student> createListOfStudents(){
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(1, "Vasya", "Pipkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DB"));
        students.add(new Student(2, "Vasya", "Pupkin", "Vasylych", 1998, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DB"));
        students.add(new Student(3, "Vasya", "Popkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Shmitrosti", "Interesnyi", "DBL"));
        students.add(new Student(4, "Vasya", "Papkin", "Vasylych", 1999, "Pupochnaya st.", "0987654433", "Ofigennoosti", "Interesnyi", "DB"));
        students.add(new Student(5, "Vasya", "Pepkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DB"));
        students.add(new Student(6, "Vasya", "Pupkun", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Electronics", "Interesnyi", "DB"));
        students.add(new Student(7, "Vasya", "Bipkin", "Vasylych", 1998, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DB"));
        students.add(new Student(8, "Vasya", "Shmipkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Shmitrosti", "Interesnyi", "DBL"));
        students.add(new Student(9, "Vasya", "Napkin", "Vasylych", 1999, "Pupochnaya st.", "0987654433", "Ofigennoosti", "Interesnyi", "DB"));
        students.add(new Student(10, "Vasya", "Pipkin", "Vasylych", 1997, "Pupochnaya st.", "0987654433", "Hitrosti", "Interesnyi", "DBK"));

        return students;
    }
}
